package com.zuitt.batch193;

import java.util.Scanner;

public class activity {

    public static void main(String[] args){
        Scanner appScanner = new Scanner(System.in);

        System.out.println("First Name:");
        String userFName = appScanner.nextLine().trim();
        System.out.println(userFName);

        System.out.println("Last Name:");
        String userLName = appScanner.nextLine().trim();
        System.out.println(userLName);

        System.out.println("First Subject Grade:");
        double firstSubj = appScanner.nextDouble();
        System.out.println(firstSubj);

        System.out.println("Second Subject Grade:");
        double secondSubj = appScanner.nextDouble();
        System.out.println(secondSubj);

        System.out.println("Third Subject Grade:");
        double thirdSubj = appScanner.nextDouble();
        System.out.println(thirdSubj);

        System.out.println("Good day, " + userFName + " " + userLName);
        double sum = (double) (firstSubj + secondSubj + thirdSubj);
        int gradeAvg = (int) (sum / 3);
        System.out.println("Your grade average is: " + gradeAvg);


    }
}
